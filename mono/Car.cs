﻿
public class CarData
{
    public CarIdData id;
    public CarDimensionData dimensions;

    public override bool Equals(object obj)
    {
        if (obj is CarData)
            return ((CarData)obj).id.color == this.id.color && ((CarData)obj).id.name == this.id.name;
        else
            return base.Equals(obj);
    }
}

public class CarIdData
{
    public string name;
    public string color;

    public override bool Equals(object obj)
    {
        if (obj is CarIdData)
            return (((CarIdData)obj).color == this.color) && ((CarIdData)obj).name == this.name;
        else
            return base.Equals(obj);
    }
}

public class CarDimensionData
{
    public double length;
    public double width;
    public double guideFlagPosition;
}
