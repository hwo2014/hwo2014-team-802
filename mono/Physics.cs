﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Physics
{
    public static List<VelocityDeltaData> _velocities = new List<VelocityDeltaData>();

    public static List<VelocityDeltaData> VelocityDeltas
    {
        get
        {
            return _velocities;
        }
    }

    public static double GetVelocityDeltaByTickAndThrottle(List<CarPositionsMessage> Ticks, CarIdData Car, TrackData track, double Throttle)
    {
        int tickCount = Ticks.Count();
        if (tickCount < 2)
            return 0d;

        double firstVelocity = GetVelocityPerTick(Ticks[0], Ticks[1], Car, track);
        double lastVelocity = GetVelocityPerTick(Ticks[Ticks.Count - 2], Ticks[Ticks.Count - 1], Car, track);

        double diff = Math.Abs(lastVelocity - firstVelocity);

        return Math.Abs(diff / tickCount);
    }

    public static double GetMaximumSlipAngle(PieceData Curve)
    {
        
        return 0.45d;
    }

    public static double GetVelocityPerTick(CarPositionsMessage PreviousTick, CarPositionsMessage CurrentTick, CarIdData car, TrackData track)
    {
        if (PreviousTick == null || CurrentTick == null || car == null || CurrentTick.gameTick == PreviousTick.gameTick || PreviousTick.gameTick > CurrentTick.gameTick)
            return 0d;

        int tickDiff = Math.Abs(CurrentTick.gameTick - PreviousTick.gameTick);

        CarPositionsData previousPos = PreviousTick.data.First(m => m.id.name == car.name && m.id.color == car.color);
        CarPositionsData currentPos = CurrentTick.data.First(m => m.id.name == car.name && m.id.color == car.color);

        if (previousPos == null || currentPos == null)
            return 0d;

        double diff = 0d;

        if (previousPos.piecePosition.pieceIndex == currentPos.piecePosition.pieceIndex)
            diff = currentPos.piecePosition.inPieceDistance - previousPos.piecePosition.inPieceDistance;
        else 
        {
            PieceData iteratorPiece = track.pieces.First(m=>m.index == previousPos.piecePosition.pieceIndex);
            diff += iteratorPiece.length.HasValue ? (iteratorPiece.length.Value - previousPos.piecePosition.inPieceDistance) : 
                (MathHelpers.GetArcLengthByAngleAndRadius(iteratorPiece.angle.Value, iteratorPiece.radius.Value) - previousPos.piecePosition.inPieceDistance);

            iteratorPiece = iteratorPiece.next;

            while (iteratorPiece.index != currentPos.piecePosition.pieceIndex)
            {
                diff += iteratorPiece.length.HasValue ? iteratorPiece.length.Value : MathHelpers.GetArcLengthByAngleAndRadius(iteratorPiece.angle.Value, iteratorPiece.radius.Value);
                iteratorPiece = iteratorPiece.next;
            }

            diff += currentPos.piecePosition.inPieceDistance;
        }
        return diff / tickDiff;
    }
}
