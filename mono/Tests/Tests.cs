﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
	[TestFixture ()]
	public class Tests
	{
		private string _host = "hakkinen.helloworldopen.com";
		private string _port = "8091";
		private string _botName = "Kallio";
		private string _botKey = "yMrwmA3eiCZfqA";

		public Tests(){}

		[Test ()]
		public void CreateRaceKeimola ()
		{
			List<string> args  = new List<string>();
			args.Add (_host);
			args.Add (_port);
			args.Add (_botName);
			args.Add (_botKey);
			args.Add ("msgType=createRace");
			args.Add ("trackName=keimola");
			args.Add ("password=sivukirjasto");
			args.Add ("carCount=1");

			string[] cmdArgs = GetCmdArgsByList (args);
			Bot.Main (cmdArgs);		
		}


		[Test ()]
		public void CreateRaceGermany ()
		{
			List<string> args  = new List<string>();
			args.Add (_host);
			args.Add (_port);
			args.Add (_botName);
			args.Add (_botKey);
			args.Add ("msgType=createRace");
			args.Add ("trackName=germany");
			args.Add ("password=sivukirjasto");
			args.Add ("carCount=1");

			string[] cmdArgs = GetCmdArgsByList (args);
			Bot.Main (cmdArgs);
		}

		[Test ()]
		public void CreateRaceUsa ()
		{
			List<string> args  = new List<string>();
			args.Add (_host);
			args.Add (_port);
			args.Add (_botName);
			args.Add (_botKey);
			args.Add ("msgType=createRace");
			args.Add ("trackName=usa");
			args.Add ("password=sivukirjasto");
			args.Add ("carCount=1");

			string[] cmdArgs = GetCmdArgsByList (args);
			Bot.Main (cmdArgs);
		}

		[Test ()]
		public void CreateRaceFrance ()
		{
			List<string> args  = new List<string>();
			args.Add (_host);
			args.Add (_port);
			args.Add (_botName);
			args.Add (_botKey);
			args.Add ("msgType=createRace");
			args.Add ("trackName=france");
			args.Add ("password=sivukirjasto");
			args.Add ("carCount=1");

			string[] cmdArgs = GetCmdArgsByList (args);
			Bot.Main (cmdArgs);
		}

		/// <summary>
		/// Gets the arguments by list.
		/// </summary>
		/// <returns>The arguments by list.</returns>
		/// <param name="list">List.</param>
		private string[] GetCmdArgsByList(List<string> list)
		{
			return list.Select (x => x).ToArray ();
		}
	}
}

