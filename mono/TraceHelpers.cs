﻿using System;

public static class TraceHelpers
{
    static bool _trace = true;
    public static bool Trace
    {
        get
        {
            return _trace;
        }
        set
        {
            _trace = value;
        }
    }
	/// <summary>
	/// Write the specified message.
	/// </summary>
	/// <param name="message">Message.</param>
	public static void Write(string message)
	{
        if (Trace)
			Console.WriteLine (
				DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss.ffff") + " *** " +	message
			);
	}


}


