﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class GameState
{
    private List<CarPositionsMessage> _positionHistory = new List<CarPositionsMessage>();
    private TrackData _currentTrack = new TrackData();

    public List<CarPositionsMessage> PositionHistory
    {
        get
        {
            return _positionHistory;
        }
    }

    public class SlipAngleData
    {
        public double CarSlipAngle;
        public double CurveAngle;
        public double Velocity;
    }

    List<SlipAngleData> _slipAngles = new List<SlipAngleData>();
    public List<SlipAngleData> SlipAngleHistory
    {
        get
        {
            return _slipAngles;
        }
    }

    public CarPositionsMessage LastPositionUpdate
    {
        get
        {
            if (PositionHistory.Count > 0)
                return PositionHistory.Last();
            else
                return null;
        }
    }

    public void StoreHistory()
    {
        try
        {
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(PositionHistory);
            System.IO.File.WriteAllText(string.Format("racehistory_{0}.txt", System.DateTime.Now.ToString("yyyy-MM-dd-hh-mm")), output);
            output = Newtonsoft.Json.JsonConvert.SerializeObject(SlipAngleHistory);
            System.IO.File.WriteAllText(string.Format("sliphistory_{0}.txt", System.DateTime.Now.ToString("yyyy-MM-dd-hh-mm")), output);
        }
        catch
        {
            // Ignore for now. Nothing especially crucial was lost.
        }
    }

    public CarData PlayerCar
    {
        get;
        set;
    }

    public AI PlayerAI
    {
        get;
        set;
    }

    public TrackData CurrentTrack
    {
        get
        {
            return _currentTrack;
        }
        set
        {
            _currentTrack = value;
        }
    }

    public TurboData Turbo
    {
        get;
        set;
    }
}
