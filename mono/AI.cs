﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class AI
{
    private double DefaultThrottle
    {
        get
        {
            return 0.60d;
        }
    }

    Path _path;

    public Path Path
    {
        get
        {
            if (_path == null && CurrentGameState.CurrentTrack != null)
                _path = CalculatePathByTrack(CurrentGameState.CurrentTrack);
            return _path;
        }
        set
        {
            _path = value;
        }
    }

    public Path CalculatePathByTrack(TrackData track)
    {
        return PathFinder.GetPathByTrackData(track);
    }

    public AI(CarData Car)
    {
        this.Car = Car;
        this.DrivingMode = Mode.HotLap;
    }

    public int CurrentLap
    {
        get;
        set;
    }

    public CarData Car
    {
        get;
        set;
    }

    public bool Crashed
    {
        get;
        set;
    }

    public GameState CurrentGameState
    {
        get;
        set;
    }

    public bool TurboAvailable
    {
        get;
        set;
    }

    private bool _measureComplete = false;
    public bool MeasureComplete
    {
        get
        {
            return _measureComplete;
        }
        set
        {
            _measureComplete = value;
        }
    }

    int measureSampleSize = 10;
    int measuresMade;
    List<CarPositionsMessage> measures = new List<CarPositionsMessage>();
    MeasureMode _measureState = MeasureMode.Acceleration;
    double accelerationThrottle = 1d;
    double decelerationThrottle = 0.2d;
    double cruiseThrottle = 0.5d;

    public double Acceleration
    {
        get;
        set;
    }

    public double Deceleration
    {
        get;
        set;
    }

    public double SafeSlipAngle
    {
        get;
        set;
    }

    public ClientMessage Think(CarPositionsMessage PositionUpdate)
    {
        double currentVelocity = Physics.GetVelocityPerTick(CurrentGameState.LastPositionUpdate, PositionUpdate, this.Car.id, this.CurrentGameState.CurrentTrack);

        if (this.CurrentLap > 0)
            MeasureComplete = true;

        if (!MeasureComplete)
        {
            switch (_measureState)
            {
                case MeasureMode.Acceleration:
                    currentThrottle = accelerationThrottle;

                    if (measuresMade >= measureSampleSize)
                    {
                        double velocityDelta = Physics.GetVelocityDeltaByTickAndThrottle(measures, Car.id, CurrentGameState.CurrentTrack, currentThrottle);
                        TraceHelpers.Write(string.Format("Measure delta: {0} with throttle: {1}", velocityDelta, currentThrottle));
                        Acceleration = velocityDelta;
                        measuresMade = 0;
                        measures.Clear();
                        _measureState = MeasureMode.Deceleration;
                    }
                    else
                    {
                        measures.Add(PositionUpdate);
                        measuresMade++;
                    }

                    return new ThrottleMessage(currentThrottle);
                case MeasureMode.Deceleration:
                    if (currentVelocity < 0.3d)
                    {
                        currentThrottle = cruiseThrottle;
                        return new ThrottleMessage(cruiseThrottle);
                    }

                    currentThrottle = decelerationThrottle;

                    if (measuresMade >= measureSampleSize)
                    {
                        double velocityDelta = Physics.GetVelocityDeltaByTickAndThrottle(measures, Car.id, CurrentGameState.CurrentTrack, currentThrottle);
                        TraceHelpers.Write(string.Format("(Deceleration) Measure delta: {0} with throttle: {1}", velocityDelta, currentThrottle));
                        Deceleration = velocityDelta;
                        measuresMade = 0;
                        measures.Clear();
                        _measureState = MeasureMode.Curve;
                    }
                    else
                    {
                        measures.Add(PositionUpdate);
                        measuresMade++;
                    }

                    return new ThrottleMessage(currentThrottle);
                case MeasureMode.Curve:
                    PieceData currentPiece = CurrentGameState.CurrentTrack.pieces.First(m => m.index == PositionUpdate.GetCurrentPieceIndexByCar(Car.id));

                    if(!currentPiece.IsStraight)
                    {
                        double currentAngle = PositionUpdate.data.First(m=>m.id.Equals(Car.id)).angle;
                        TraceHelpers.Write(string.Format("Angle: {0} Curve angle: {1} / Velocity: {2}", currentAngle,
                                                                currentPiece.angle, Physics.GetVelocityPerTick(CurrentGameState.PositionHistory.Skip(CurrentGameState.PositionHistory.Count-2).First(), 
                                                                CurrentGameState.PositionHistory.Skip(CurrentGameState.PositionHistory.Count-1).First(),Car.id, CurrentGameState.CurrentTrack)));

                        if (Math.Abs(currentAngle) <= Math.Abs(SafeSlipAngle - 10))
                        {
                            currentThrottle += 0.02d;
                        }
                        else
                        {
                            currentThrottle -= 0.02d;
                        }

                        if (!Crashed && currentAngle > 0 && currentAngle > SafeSlipAngle)
                        {
                            SafeSlipAngle = currentAngle;
                            TraceHelpers.Write("Safe angle updated to: " + SafeSlipAngle);
                        }

//                        currentThrottle = DefaultThrottle;
//                        CurrentGameState.PositionHistory.Skip(CurrentGameState.PositionHistory.Count - measureSampleSize);
                    }

                    if (currentThrottle > 1.0)
                        currentThrottle = 1.0;
                    return new ThrottleMessage(currentThrottle);
            }
        }

        if (currentVelocity == 0d)
        {
            return new ThrottleMessage(1.0);
        }

        if (CurrentGameState.CurrentTrack.pieces.First(m=>m.index == PositionUpdate.GetCurrentPieceIndexByCar(Car.id)).IsStartOfStraight)
        {
            TraceHelpers.Write("start of a straight.. Piece index: " + PositionUpdate.GetCurrentPieceIndexByCar(Car.id));
        }
//        if (TurboAvailable && CurrentGameState.CurrentTrack.patterns)

        // Think about what to do
        // return the appropriate response
        if (this.CurrentLap == 0)
            this.DrivingMode = Mode.Measure;
        else
            this.DrivingMode = Mode.HotLap;

        ClientMessage action;

        switch (DrivingMode)
        {
            case Mode.Turbo:
                action = Turbo(PositionUpdate);
                break;
            case Mode.Start:
                action = Start(PositionUpdate);
                break;
            case Mode.HotLap:
                action = HotLap(PositionUpdate);
                break;
            case Mode.Race:
                action = Race(PositionUpdate);
                break;
            case Mode.Measure:
                action = Measure(PositionUpdate);
                break;
            case Mode.Overtake:
                action = OverTake(PositionUpdate);
                break;
            default:
                action = new PingMessage();
                break;
        }

        return action;
    }

    int _pieceIndex = 0;

    public ClientMessage Measure(CarPositionsMessage carPositionMessage)
    {
        double slipAngle = carPositionMessage.data.First(m => m.id.Equals(this.Car.id)).angle;
        return HotLap(carPositionMessage);
    }

    List<string> _taunts = new List<string>() 
                { 
                    "Eat dust",
                    "Did I do that?",
                    "Crash...?"
                };

    static Random rnd = new Random();

    public ClientMessage Turbo(CarPositionsMessage carPositionMessage)
    {
        TurboAvailable = false;
        return new TurboMessage(_taunts[rnd.Next(_taunts.Count)]);
    }

    public ClientMessage OverTake(CarPositionsMessage carPositionMessage)
    {
        return HotLap(carPositionMessage);
    }

    public ClientMessage Race(CarPositionsMessage carPositionMessage)
    {
        return HotLap(carPositionMessage);
    }

    public ClientMessage HotLap(CarPositionsMessage carPositionMessage)
    {
        var myCar = carPositionMessage.data
			.Single (x => x.id.name.Equals(CurrentGameState.PlayerCar.id.name) &&
		            x.id.color.Equals (CurrentGameState.PlayerCar.id.color));

        if (CurrentGameState.LastPositionUpdate == null)
            CurrentGameState.PositionHistory.Add(carPositionMessage);

		//Check if the piece changed
		if (_pieceIndex < myCar.piecePosition.pieceIndex) {
			//Flush the history
            this.PreviousAction = null;
		}
		_pieceIndex = myCar.piecePosition.pieceIndex;

		//TraceHelpers.Write(string.Format("Current velocity: {0}, slip angle: {1}, curve angle: {2}", Physics.GetVelocityPerTick(CurrentGameState.LastPositionUpdate, carPositionMessage, myCar.id),
		//                myCar.angle, CurrentGameState.CurrentTrack.pieces.First(m=>m.index == _pieceIndex).angle));

        if (Math.Abs(myCar.angle) != 0 && !CurrentGameState.CurrentTrack.pieces.First(m => m.index == _pieceIndex).IsStraight)
            CurrentGameState.SlipAngleHistory.Add(new GameState.SlipAngleData()
            {
                CarSlipAngle = myCar.angle,
                CurveAngle = CurrentGameState.CurrentTrack.pieces.First(m => m.index == _pieceIndex).angle.Value,
                Velocity = Physics.GetVelocityPerTick(CurrentGameState.LastPositionUpdate, carPositionMessage, myCar.id, CurrentGameState.CurrentTrack)
            });
		//Decide if to switch the lane

        if (!(PreviousAction is SwitchLaneMessage))
        {
            //Look forward for the preferable lane (the next sequence may contain a switch)
            int laneTargetPosition = myCar.piecePosition.pieceIndex + 2;
            //Get the preferable lane from the path
            int targetLane = Path.GetPreferableLaneByPieceIndex(laneTargetPosition);

            if (targetLane > myCar.piecePosition.lane.startLaneIndex)
            {
                return new SwitchLaneMessage(SwitchLaneDirection.Right);
            }

            if (targetLane < myCar.piecePosition.lane.startLaneIndex)
            {
                return new SwitchLaneMessage(SwitchLaneDirection.Left);
            }
        }

		//Otherwise, adjust throttling
		//Straight piece
        PieceData current = this.CurrentGameState.CurrentTrack.pieces[myCar.piecePosition.pieceIndex];

        currentThrottle = DefaultThrottle;

        return new ThrottleMessage(currentThrottle);
    }

    public ClientMessage Start(CarPositionsMessage PositionUpdate)
    {
        return new ThrottleMessage(DefaultThrottle);
    }

    public Double currentThrottle
    {
        get;
        set;
    }

    public ClientMessage PreviousAction
    {
        get;
        set;
    }

    public Mode DrivingMode
    {
        get;
        set;
    }

    public enum Mode
    {
        Measure,
        HotLap,
        Race,
        Turbo,
        Overtake,
        Start
    }

    public enum MeasureMode
    {
        Acceleration,
        Deceleration,
        Curve
    }
}

