﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

public static class PathFinder
{
	/// <summary>
	/// Gets the path by track data.
	/// </summary>
	/// <returns>The path by track data. The dictionary contains preferred lane by piece index.</returns>
	/// <param name="trackData">Track data.</param>
	public static Path GetPathByTrackData(TrackData trackData)
	{
		ReaderWriterLockSlim locker = new ReaderWriterLockSlim();
		List<Path> roots = new List<Path> ();

		bool processParallel = System.Environment.ProcessorCount > 1;

		Stopwatch watch = new Stopwatch ();
		watch.Start ();

		//Search for all lanes (at start)
		if (processParallel) {
			System.Threading.Tasks.Parallel.ForEach (trackData.lanes, lane => {
				Path path = GetPath (0, lane.index, 0, trackData);
				locker.EnterWriteLock ();
				try {
					roots.Add (path);
				} finally {
					locker.ExitWriteLock ();
				}
			});
		} else {
			//If no parallelism is available, threading causes just excess overhead...
			foreach (var lane in trackData.lanes) {
				roots.Add (GetPath (0, lane.index, 0, trackData));
			}
		}

		//Select the path with the lowest cost
		Path retval = roots.LastOrDefault (x => x.cumulativeCost == roots.Min (y => y.cumulativeCost));
		watch.Stop();

		TraceHelpers.Write (
			String.Format ("GetPathByTrackData executed {0} in {1} milliseconds", 
				processParallel ? "parallel" : "sequentially", watch.Elapsed.TotalMilliseconds)
		);

		retval.PrintPath ();

		return retval;
	}

	/// <summary>
	/// Gets the path by using recursive algorithm.
	/// </summary>
	/// <returns>The path.</returns>
	/// <param name="seq">Seq.</param>
	/// <param name="lane">Lane.</param>
	/// <param name="cumulativeCost">Cumulative cost.</param>
	/// <param name="trackData">Track data.</param>
	private static Path GetPath(int seq, int lane, double cumulativeCost, TrackData trackData)
	{	
		//Todo, find out if there is any cost for switching the lane
		double switchingCost = 5; 

		List<Path> paths = new List<Path> ();

		//Calculate the cost of the current block
		double cost = double.MaxValue;

		//Straight block
		if (trackData.pieces [seq].length != null) {
			cost = (double)trackData.pieces [seq].length;
		}
		//Bended blocks
		else if (trackData.pieces [seq].angle != null && trackData.pieces [seq].radius != null) {		
			double radius = (trackData.pieces [seq].angle < 0) ? 
				(double)(trackData.pieces [seq].radius + trackData.lanes [lane].distanceFromCenter) : 
				(double)(trackData.pieces [seq].radius - trackData.lanes [lane].distanceFromCenter);
			cost = MathHelpers.GetArcLengthByAngleAndRadius ((double)trackData.pieces [seq].angle, radius);
		} 

		//Decide recursive calls
		//This is the last piece
		if (seq >= trackData.pieces.Count - 1) {
			//Return the complete path
			Path path = new Path ();
			//Add the cumulative cost to the path (the best path is selected by using the cost)
			path.cumulativeCost = cumulativeCost + cost;
			//Add the current node to the path
			path.AddPathNode (seq, lane);
			return path;

		} else if (trackData.pieces [seq].isSwitch == true) {
			//Find the next switch position and collect left and right turns in order 
			//to reduce the number of recursions
			bool switchFound = false, leftTurnAhead = false, rightTurnAhead = false;
			//Start the search from the next element
			int idx = seq;
			do {
				//The transition from the last piece to the first
				if (++idx == trackData.pieces.Count)
					idx = 0;
				//Turns between the switches
				if (trackData.pieces [idx].angle != null) {
					if (trackData.pieces [idx].angle < 0)
						leftTurnAhead = true;

					if (trackData.pieces [idx].angle > 0)
						rightTurnAhead = true;

					//If the clip of the track has already had left turns and right turns, 
					//no need to go further...
					if(leftTurnAhead == rightTurnAhead == true) 
						break;
				}
				//If the switch is reached
				if (trackData.pieces [idx].isSwitch == true)
					switchFound = true;
			} while(!switchFound);				

			//Switch to left		
			if (leftTurnAhead) {
				if (lane > trackData.lanes.Min (x => x.index)) {
					Path leftChild = GetPath (seq + 1, lane - 1, cumulativeCost + switchingCost + cost, trackData);
					//Add the current node to the path
					leftChild.AddPathNode (seq, lane);
					paths.Add (leftChild);
				}
			}
			//Switch to right
			if (rightTurnAhead) {
				if (lane < trackData.lanes.Max (x => x.index)) {			
					Path rightChild = GetPath (seq + 1, lane + 1, cumulativeCost + switchingCost + cost, trackData);
					//Add the current node to the path
					rightChild.AddPathNode (seq, lane);
					paths.Add (rightChild);
				}
			}
		}
		//Go forward without switching
		Path middleChild = GetPath(seq + 1, lane, cumulativeCost + cost, trackData);
		middleChild.AddPathNode (seq, lane);
		paths.Add (middleChild);

		Path retval = paths.LastOrDefault(x => x.cumulativeCost == paths.Min (y => y.cumulativeCost));
		//Return the path with the smallest total cost
		return retval;
	}
}

public class Path
{
	/// <summary>
	/// Total cost of the path
	/// </summary>
	public double cumulativeCost { get; set; }

	/// <summary>
	/// Piece sequence, lane index
	/// </summary>
	private Dictionary<int,int> _path = new Dictionary<int,int> ();

	/// <summary>
	/// Adds the path node.
	/// </summary>
	/// <param name="sequence">Sequence.</param>
	/// <param name="laneIndex">Lane index.</param>
	public void AddPathNode(int sequence, int laneIndex)
	{
		_path.Add (sequence, laneIndex);
	}

	/// <summary>
	/// Gets the preferred lane by sequence.
	/// </summary>
	/// <returns>The preferred lane by sequence.</returns>
	/// <param name="seq">Seq.</param>
	public int GetPreferableLaneByPieceIndex(int pieceIndex)
	{
		int retval;

		//Normalize the index
		int seq = pieceIndex - (int)Math.Floor ((decimal)(pieceIndex / _path.Count)) * (_path.Count); 

		if (_path.ContainsKey (seq)) {
			retval = _path[seq];
		} else {
			//Find the rightmost lane in case of the key is not found from the dictionary
			//This is not to happen unless the path is corrupted
			retval = _path.Values.Max ();
		}

		return retval;
	}

	/// <summary>
	/// Prints the path
	/// </summary>
	public void PrintPath()
	{
		string pathInfo = String.Empty;

		pathInfo = @"** Path information
Cumulative cost: " + this.cumulativeCost + @"
		
Piece" + "\t\t" + "Lane\t" + @"
------------------------------------
";
		foreach (int key in _path.Keys.OrderBy(x => x)) {
			int lane = _path [key];		

			pathInfo += key + "\t" + ">" + "\t" + lane + "\n";
		}

		Console.WriteLine (pathInfo);
	}

}


