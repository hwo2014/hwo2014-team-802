﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TrackData
{
    public string id;
    public string name;
    public List<PieceData> pieces = new List<PieceData>();
    public List<LaneData> lanes = new List<LaneData>();
    public StartingPointData startingPoint;
    public IEnumerable<IGrouping<bool, PieceData>> patterns = null;
    public VelocityDeltaData velocityData;

    public double length = 0d;

    public void CalculateTrackMetrics()
    {
        // total length of the track.
        length = pieces.Sum(m => m.length ?? (MathHelpers.GetArcLengthByAngleAndRadius(m.angle.Value, (double)m.radius.Value)));

        if (pieces != null)
            patterns = pieces.GroupAdjacent(m => m.angle != null);

        // calculate the straights.
        foreach (IGrouping<bool, PieceData> pattern in patterns)
        {
            PieceData firstPiece = pieces.First(m => m.index == pattern.First().index);
            firstPiece.IsStartOfStraight = pattern.First().IsStraight;
            firstPiece.StraightLength = pattern.Count();

            pieces.Last(m => m.index == pattern.Last().index).IsEndOfStraigth = pattern.Last().IsStraight;
        }

        int pieceCount = pieces.Count;

        // Link up the pieces to the adjacent ones.
        for (int i = 0; i < pieceCount; i++)
        {
            int modifier = 0;

            pieces[i].index = i;
            modifier = (i >= (pieceCount - 1)) ? 0 : i + 1;

            pieces[i].next = pieces[modifier];
        }
    }
}

public class VelocityDeltaData
{
    public double VelocityByTick;
    public double Throttle;
}

public class PieceData
{
    public double? length;
    public double? angle;
    [Newtonsoft.Json.JsonProperty("switch")]
    public bool? isSwitch;
    public int? radius;
    public PieceData next;
    public int? index;
    private bool isStartOfStraight = false;
    private bool isEndOfStraight = false;

    public bool IsStraight
    {
        get
        {
            return length.HasValue && !angle.HasValue;
        }
    }
    public bool IsStartOfStraight
    {
        get
        {
            return isStartOfStraight;
        }
        set
        {
            isStartOfStraight = value;
        }
    }

    public bool IsEndOfStraigth
    {
        get
        {
            return isEndOfStraight;
        }
        set
        {
            isEndOfStraight = value;
        }
    }

    public int StraightLength
    {
        get;
        set;
    }
}

public class LaneData
{
    public int distanceFromCenter;
    public int index;
}

public class StartingPointData
{
    public PositionData position;
    public double angle;
}