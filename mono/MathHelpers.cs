﻿using System;


public static class MathHelpers
{
	/// <summary>
	/// Gets the arc lenght by angle and radius.
	/// </summary>
	/// <returns>The arc lenght by angle and radius.</returns>
	/// <param name="angle">Angle.</param>
	/// <param name="radius">Radius.</param>
	public static double GetArcLengthByAngleAndRadius(double angle, double radius)
	{
		return Math.Abs (angle) / 360 * 2 * Math.PI * radius;
	}

}

