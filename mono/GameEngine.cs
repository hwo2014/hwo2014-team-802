﻿using System;
using System.Collections.Generic;
using System.Linq;

public class GameEngine
{
    private GameState _state;

    public GameState State
    {
        get
        {
            return _state;
        }
        private set
        {
            _state = value;
        }
    }

    public GameEngine (GameInitMessage initMessage, YourCarMessage carMessage)
	{
        this.State = new GameState();
        this.State.CurrentTrack = initMessage.data.race.track;
        this.State.CurrentTrack.CalculateTrackMetrics();

        this.State.PlayerCar = new CarData() { id = new CarIdData() { color = carMessage.data.color, name = carMessage.data.name } };
        this.State.PlayerAI = new AI(State.PlayerCar);
        this.State.PlayerAI.CurrentGameState = State;
		//Mode.Measure / Mode.HotLap
		this.State.PlayerAI.DrivingMode = initMessage.GetRaceMode ();
	}

	/// <summary>
	/// Reinitialize the specified initMessage.
	/// </summary>
	/// <param name="initMessage">Init message.</param>
	public void Reinitialize (GameInitMessage initMessage){
		this.State.CurrentTrack = initMessage.data.race.track;
		this.State.CurrentTrack.CalculateTrackMetrics();
		//Mode.Measure / Mode.HotLap
		this.State.PlayerAI.DrivingMode = initMessage.GetRaceMode ();
	}

	public ClientMessage Update(CarPositionsMessage carPositionMessage)
	{
        if (_state.LastPositionUpdate == null)
            _state.PositionHistory.Add(carPositionMessage);
        _state.PlayerAI.CurrentLap = carPositionMessage.data.Where(m => m.id.Equals(_state.PlayerCar.id)).First().piecePosition.lap;
        ClientMessage returnMessage = _state.PlayerAI.Think(carPositionMessage);

        if (State.LastPositionUpdate.gameTick != carPositionMessage.gameTick)
            State.PositionHistory.Add(carPositionMessage);

        State.PlayerAI.PreviousAction = returnMessage;

        return returnMessage;
	}

    public void UpdateTurbo(TurboData Turbo)
    {
        this.State.Turbo = Turbo;
        this.State.PlayerAI.TurboAvailable = true;
    }
}

