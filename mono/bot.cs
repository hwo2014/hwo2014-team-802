using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;

public class Bot
{

	/// <summary>
	/// The entry point of the program, where the program control starts and ends.
	/// </summary>
	/// <param name="args">The command-line arguments.</param>
	public static void Main (string[] args)
	{

		if (args.Length < 4) {
			Console.WriteLine (
				@"

Usage: bot.exe [host] [port] [bot name] [bot key]

Additional arguments to run test races:

msgType=[initial message sent to the server] (createRace or joinRace)
trackName=[name of the track]
password=[track password]
carCount=[number of cars in the track]

Examples:
./bot.exe hakkinen.helloworldopen.com 8091 Kallio 1234567890
./bot.exe hakkinen.helloworldopen.com 8091 Kallio 1234567890 msgType=createRace trackName=germany password=Kaarle carCount=1

"
			);

			return;		
		}
			
		//Parse command line arguments
		string host = args [0];
		int port = int.Parse (args [1]);
		string botName = args [2];
		string botKey = args [3];

		//Additional command line parameters to run test races
		string msgType = (args.SingleOrDefault (x => x.Contains ("msgType=")) ?? "").Split ('=').Last ();
		string trackName = (args.SingleOrDefault (x => x.Contains ("trackName=")) ?? "").Split ('=').Last ();
		string password = (args.SingleOrDefault (x => x.Contains ("password=")) ?? "").Split ('=').Last ();
		string carCount = (args.SingleOrDefault (x => x.Contains ("carCount=")) ?? "").Split ('=').Last ();
        string trace = (args.SingleOrDefault(x => x.Contains("trace")) ?? "").Split('=').Last();

        bool traceResult = false;
        if (!string.IsNullOrWhiteSpace(trace) && bool.TryParse(trace, out traceResult))
            TraceHelpers.Trace = traceResult;

		Console.WriteLine ("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using (TcpClient client = new TcpClient (host, port)) {
			NetworkStream stream = client.GetStream ();
			StreamReader reader = new StreamReader (stream);
			StreamWriter writer = new StreamWriter (stream);
			writer.AutoFlush = true;

			ClientMessage msgInit;

			switch (msgType) {
			case MessageType.CreateRace:
				msgInit = new CreateRaceMessage (botName, botKey, trackName, password, Convert.ToInt32 (carCount));
				break;

			case MessageType.JoinRace:
				msgInit = new JoinRaceMessage (botName, botKey, trackName, password, Convert.ToInt32 (carCount));
				break;

			default:
				msgInit = new JoinMessage (botName, botKey);
				break;
			}
				
			new Bot (reader, writer, msgInit);
		}
	}

	private StreamWriter writer;
    public GameEngine engine;

	Bot (StreamReader reader, StreamWriter writer, ClientMessage join)
	{
		this.writer = writer;
		string line;
		 
		send (join);
		var myCarMessage = WaitForMessage<YourCarMessage> (reader, MessageType.YourCar);
		var initMessage = WaitForMessage<GameInitMessage> (reader, MessageType.GameInit);

		//Todo, better error handling
		//Server may return an error message instead of game init
		if (initMessage == null) {
			throw new Exception ("Failed to initialize the game, no game init message was received!");
		} 

		engine = new GameEngine (initMessage, myCarMessage);
		Console.WriteLine ("Game initialized");
		double previousAngle = 0d;

		while ((line = reader.ReadLine ()) != null) {
			var msg = JsonConvert.DeserializeObject<GenericMessage> (line);
			switch (msg.msgType) {
			case MessageType.GameInit:
				//If a second game init is received, the first round was a qualifying session
				initMessage = JsonConvert.DeserializeObject<GameInitMessage> (line);
				//Check if the qualifying session is over
				engine.Reinitialize (initMessage);
				break;
			case MessageType.CarPositions:
				var positionMessage = JsonConvert.DeserializeObject<CarPositionsMessage> (line);
				previousAngle = positionMessage.data [0].angle;
				send (engine != null ? engine.Update (positionMessage) : new PingMessage ());
				break;
			case MessageType.Join:
				Console.WriteLine ("Joined");
				send (new PingMessage ());
				break;
			case MessageType.GameEnd: 
				Console.WriteLine ("Race ended");
				send (new PingMessage ());
				engine.State.StoreHistory ();
				break;
			case MessageType.TournamentEnd:
				Console.WriteLine ("Tournament ended");
				//Sould we exit or not? Maybe the server will close the connection...
				send (new PingMessage ());
				break;
			case MessageType.GameStart:
				Console.WriteLine ("Race starts");
				//No need to call the path finder here
				//Path is initialized in AI.cs:132
				//if (engine != null)
				//	engine.State.PlayerAI.CalculatePathByTrack (engine.State.CurrentTrack);
				send (new PingMessage ());
				break;
			case MessageType.Crash:
				var crashMessage = JsonConvert.DeserializeObject<CrashMessage> (line);
                if (crashMessage.data.Equals(engine.State.PlayerCar))
                {
                    engine.State.PlayerAI.Crashed = true;
                }
				Console.WriteLine ("A car crashed. Name: " + crashMessage.data.name + " Previous known slip angle: " + previousAngle);
				send (new PingMessage ());
				break;
			case MessageType.Spawn:
				var spawnMessage = JsonConvert.DeserializeObject<SpawnMessage> (line);
                if (spawnMessage.data.Equals(engine.State.PlayerCar))
                {
                    engine.State.PlayerAI.Crashed = false;
                }
				Console.WriteLine ("A car spawned. Name: " + spawnMessage.data.name);
				break;
			case MessageType.TurboAvailable:
				var turboMessage = JsonConvert.DeserializeObject<TurboAvailableMessage> (line);
				if (engine != null)
					engine.UpdateTurbo (turboMessage.data);
				Console.WriteLine (string.Format ("Turbo available. Duration(ms): {0}, ticks: {1}, factor: {2}", turboMessage.data.turboDurationMilliseconds, 
					turboMessage.data.turboDurationTicks, turboMessage.data.turboFactor));
				break;
            case MessageType.TurboStart:
                var turboStartMessage = JsonConvert.DeserializeObject<TurboStartMessage>(line);
                TraceHelpers.Write(string.Format("Turbo started by {0}", turboStartMessage.data.name));
                break;
            case MessageType.TurboEnd:
                var turboEndMessage = JsonConvert.DeserializeObject<TurboEndMessage>(line);
                TraceHelpers.Write(string.Format("Turbo ended by {0}", turboEndMessage.data.name));
                break;
            case MessageType.Error:
				TraceHelpers.Write ("Server error: " + msg.data.ToString ());
				break;
			default:
				send (new PingMessage ());
				break;
			}
		}
	}

	private static T WaitForMessage<T> (StreamReader reader, string message) where T: class
	{
		string line;
		while ((line = reader.ReadLine ()) != null) {
			if (JsonConvert.DeserializeObject<GenericMessage> (line).msgType == message) {
				var obj = JsonConvert.DeserializeObject<T> (line);
				return obj;
			}
		}
		return null;
	}

	private void send (ClientMessage msg)
	{
		string json = msg.ToJson ();
		writer.WriteLine (json);
	}

}
	
	
	
