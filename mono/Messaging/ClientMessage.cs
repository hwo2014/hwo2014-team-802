﻿using System;
using Newtonsoft.Json;


public abstract class ClientMessage {
	public string ToJson() {
		return JsonConvert.SerializeObject(new GenericMessage(this.MsgType(), this.MsgData()));
	}

	protected virtual Object MsgData() {
		return this;
	}

	protected abstract string MsgType();
}

