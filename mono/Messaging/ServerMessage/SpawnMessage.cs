﻿using System;
using System.Collections.Generic;

public class SpawnMessage : ServerMessageBase
{
    public CarIdData data;
    public string gameId;
    public int gameTick;
}
