﻿using System;
using System.Collections.Generic;

public class YourCarMessage : ServerMessageBase
{
	public YourCarData data;
}

public class YourCarData
{
	public string name;
	public string color;
}
