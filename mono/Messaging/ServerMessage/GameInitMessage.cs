﻿using System;
using System.Collections.Generic;

public class GameInitMessage : ServerMessageBase
{
	public GameData data;

	/// <summary>
	/// Gets the race mode by race session data
	/// </summary>
	/// <returns>The race mode.</returns>
	public AI.Mode GetRaceMode()
	{
		AI.Mode retval;
		RaceSessionData raceSessionData = data.race.raceSession;
		if (raceSessionData.laps == null) {
			retval = AI.Mode.Measure;
			TraceHelpers.Write ("Setting drive mode to measure");
		} else {
			TraceHelpers.Write ("Setting drive mode to hot lap");
			retval = AI.Mode.HotLap;
		}
		return retval;
	}
}

public class GameData
{
	public GameInitData race;
}

public class GameInitData
{
	public TrackData track;
	public List<CarData> cars;
	public RaceSessionData raceSession;
}

public class PositionData
{
	public double x;
	public double y;
}

public class RaceSessionData
{
	public int? durationMs;
	public int? laps;
	public int? maxLapTimeMs;
	public bool? quickRace;
}


