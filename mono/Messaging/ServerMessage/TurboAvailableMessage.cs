﻿using System;
using System.Collections.Generic;

public class TurboAvailableMessage : ServerMessageBase
{
    public TurboData data;
}

public class TurboData
{
    public double turboDurationMilliseconds;
    public double turboDurationTicks;
    public double turboFactor;
}