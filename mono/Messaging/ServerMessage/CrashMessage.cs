﻿using System;
using System.Collections.Generic;

public class CrashMessage : ServerMessageBase
{
    public CarIdData data;
    public string gameId;
    public int gameTick;
}
