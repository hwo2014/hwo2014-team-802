﻿using System;
using System.Collections.Generic;
using System.Linq;

public class CarPositionsMessage : ServerMessageBase
{
	public List<CarPositionsData> data;
	public string gameId;
	public int gameTick;

    public int GetCurrentPieceIndexByCar(CarIdData Car)
    {
        return data.First(m => m.id.Equals(Car)).piecePosition.pieceIndex;
    }

    public CarPositionsData GetCarPositionByCar(CarIdData Car)
    {
        return data.First(m => m.id.Equals(Car));
    }
}

public class CarPositionsData
{
	public CarIdData id;
	public double angle;
	public PiecePositionData piecePosition;
}

public class PiecePositionData
{
	public int pieceIndex;
	public double inPieceDistance;
	public PieceLaneData lane;
	public int lap;
}

public class PieceLaneData
{
	public int startLaneIndex;
	public int endLaneIndex;
}