﻿using System;

//How to enumerate the values in a cleaner way???
public static class MessageType {
	public const string Join = "join";
	public const string YourCar = "yourCar";
	public const string GameInit = "gameInit";
	public const string GameStart = "gameStart";
	public const string CarPositions = "carPositions";
	public const string Throttle = "throttle";
	public const string GameEnd = "gameEnd";
	public const string Crash = "crash";
	public const string Spawn = "spawn";
	public const string LapFinished = "lapFinished";
	public const string Disqualified = "dnf";
	public const string Finish = "finish";
	public const string SwitchLane = "switchLane";
	public const string TurboAvailable = "turboAvailable";
	public const string Turbo = "turbo";
	public const string CreateRace = "createRace";
	public const string JoinRace = "joinRace";
	public const string Ping = "ping";
	public const string Error = "error";
    public const string TurboStart = "turboStart";
    public const string TurboEnd = "turboEnd";
	public const string TournamentEnd = "tournamentEnd";
}