﻿using System;


public class GenericMessage : ServerMessageBase
{
	public Object data;

	public GenericMessage(string msgType, Object data) {
		this.msgType = msgType;
		this.data = data;
	}
}

