﻿using System;


public class TurboMessage: ClientMessage {
	public string message;

    public TurboMessage(string message)
    {
		this.message = message;
	}

	protected override Object MsgData() {
		return this.message;
	}

	protected override string MsgType() {
		return MessageType.Turbo;
	}
}


