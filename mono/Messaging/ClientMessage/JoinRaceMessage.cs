﻿using System;

public class JoinRaceMessage : ClientMessage {

	public BotId botId;
	public string trackName;
	public string password;
	public int carCount;

	public JoinRaceMessage() {
		botId = new BotId ();
	}

	public JoinRaceMessage(string botName, string botKey, string trackName, string password, int carCount)
	{
		botId = new BotId ();
		this.botId.name = botName;
		this.botId.key = botKey;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType ()
	{
		return MessageType.JoinRace;
	}
		
	public class BotId
	{
		public string name;
		public string key;
	}
}
