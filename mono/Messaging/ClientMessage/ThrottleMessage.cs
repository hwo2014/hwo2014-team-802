﻿using System;


public class ThrottleMessage: ClientMessage {
	public double value;

	public ThrottleMessage(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return MessageType.Throttle;
	}
}


