﻿using System;

public enum SwitchLaneDirection
{
	Left,
	Right
}

public class SwitchLaneMessage : ClientMessage
{
	SwitchLaneDirection direction;

	public SwitchLaneMessage (SwitchLaneDirection direction)
	{
		this.direction = direction;
	}
		
	protected override object MsgData ()
	{
		return direction.ToString();
	}

	protected override string MsgType ()
	{
		return MessageType.SwitchLane;
	}
}

