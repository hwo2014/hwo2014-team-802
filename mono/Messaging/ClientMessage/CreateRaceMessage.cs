﻿using System;

public class CreateRaceMessage : ClientMessage {

	public BotId botId;
	public string trackName;
	public string password;
	public int carCount;

	public CreateRaceMessage() {
		botId = new BotId ();
	}

	public CreateRaceMessage(string botName, string botKey, string trackName, string password, int carCount)
	{
		botId = new BotId();
		this.botId.name = botName;
		this.botId.key = botKey;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType ()
	{
		return MessageType.CreateRace;
	}		

	public class BotId
	{
		public string name;
		public string key;
	}
}
