﻿using System;

	public class JoinMessage: ClientMessage {
		public string name;
		public string key;
		public string color;

		public JoinMessage(string name, string key) {
			this.name = name;
			this.key = key;
			this.color = "red";
		}

		protected override string MsgType() { 
		return MessageType.Join;
		}
	}


