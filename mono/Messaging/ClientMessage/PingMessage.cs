﻿using System;

public class PingMessage: ClientMessage {
	protected override string MsgType() {
		return MessageType.Ping;
	}
}

